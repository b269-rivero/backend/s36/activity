const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoutes");

const app = express();
const port = 3001;

app.use(express.json());
app.use(express.urlencoded({extended: true }));

mongoose.connect("mongodb+srv://rizaayson:admin123@zuitt-bootcamp.jls3rmq.mongodb.net/s36?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once ("open", () => console.log(`Now connect to the cloud database!`));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}.`));