const Task = require("../models/task");

module.exports.getAllTasks = (taskID) => {
	return Task.find(taskId).then(result => {
		return result;
	})
};

module.exports.createTask = (requestBody) => {
	let newTask = new Task ({
		name: requestBody.name
	})
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskID) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			return "Deleted Task."
		}

	})
}