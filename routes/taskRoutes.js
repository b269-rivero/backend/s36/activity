const express = require("express");

const router = express.Router();

const taskController = require("../controlers/taskController");

router.get ("/:id", (req, res) => {
	taskController.getAllTasks(req.params.id).then(resultFromController => res.send(resultFromController));
});


router.post("/", (req, res) =>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

router.delete("/:id", (req, res) =>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

module.exports = router;